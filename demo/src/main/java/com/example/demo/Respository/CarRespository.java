package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Car;

public interface CarRespository extends JpaRepository<Car , Long> {
    Car findByCarCode(String carCode);
}
